#include <stdlib.h>								//system imports
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "fileLoader.h"							//the header file import for the code that was provided



bool howManyLines(char* array, int maxLines)		//boolean method for number of lines in buffer
{

 char lines = array[0];								//store 'ls' lines in an array
 int counter, lineCounter = 0;						//count variables

 while(lines != '\0' && lineCounter <= maxLines + 1)		//loop through number of lines
 {
    lines = array[counter++];						//increment array with number of lines

    if(lines == '\n')								//if there is a new line increase the
    {												//counter.
      lineCounter++;
    }
 }

 return lineCounter <= maxLines;					//return if less then max number of lines specified...
 													//in this case '25'

}



int main(int argc, char** argv)
{
  int fd[2];										//new file descriptor
  char* buffer;										//new buffer
  int pid;											//variable for forking
  pipe(fd);											//pass filedescriptor through pipe	
  
  pid = fork();										//assign pid to fork() command

  switch(pid)										//case/switch for forking
  {
    case 0:											//CHILD PROCESS
                close(fd[0]);						//close the pipe thats reading

                close(1);							//close STDOUT

                dup2(fd[1],1);						//put contents of STDOUT into the writing pipe

                execv("/bin/ls", argv); 			//execute ls

                break;								//end case

    case -1:										//case for a failed fork
                printf("FAILED");					//print an error message

                break;								//end case

    default:										//PARENT PROCESS

                wait(&pid);							//wait on CHILD PROCESS to fill pipe

                close(fd[1]);						//the write pipe must now be closed

                close(0);							//close STDIN

                buffer = loadFile(fd[0]);			//load buffer to print out CHILD PROCESS

                if(howManyLines(buffer, 25))		//do a check to see how many lines there are
                {
                    puts(buffer);					//print whats in the buffer. ie. the output of 'ls'
                }
                else								//if there are more than 25 lines...
                {
					printf("too many files");		//give an error message
                }

                break;								//end case
  }

 return EXIT_SUCCESS;								//end program. SUCCESS. WOOOHHHHOOOOOOOOO!!!!!!!

}
