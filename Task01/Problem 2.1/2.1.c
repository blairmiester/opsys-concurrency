
#include <stdio.h>
#include <time.h>     //System file used to get the time.
#include <syslog.h>

int main ()
{
  time_t rawtime;         //Initialise rawtime to a time type.
  struct tm * timeinfo;   //Creates structure to timeinfo.

   int i;                       
   for (i = 0; i < 10; i++){       //Loops only for ten iterations. If you remove the loop this would go on
									//indefinately.
   time ( &rawtime );
   timeinfo = localtime ( &rawtime );
   printf ( "Current local time and date: %s", asctime (timeinfo) ); //Converts the timeinfo from date to string

        sleep(10);  //Pauses for ten seconds before outputting the next current time.
        }

  return 0;
}
