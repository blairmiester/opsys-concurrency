#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

char* loadFile(int fd) {
	char* buf = NULL;
	char* bufp;
	size_t bufsz, cursz, curpos;
    
	ssize_t ssz;
	struct stat st;
    
	if(fstat(fd, &st) >= 0){
		bufsz = (size_t) st.st_blksize;
	} else {
		
	}
    
	buf = (char*) malloc(bufsz);
    
	curpos = 0;
	cursz = bufsz;
    
	while((ssz = read(fd, buf + curpos, bufsz)) > 0){
		curpos += ssz;
		cursz = curpos + bufsz;
		if(NULL == (bufp = (char*) realloc(buf, cursz))){
			
		}
		buf = bufp;
	}
	buf[curpos] = '\0';
	return buf;
}

