
#include <stdio.h>   
#include <sys/types.h>   //Includes system data types.
#include <unistd.h>  
#include <stdlib.h>  //Standard C library.
#include <errno.h>   
#include <time.h>         //System file used to get the time.
#include <syslog.h>
 
int main(void)
{
	time_t time_raw_format;    //Initialise rawtime to a time type.
	struct tm * ptr_time;      //Creates structure to timeinfo.
    pid_t  pid;                //Create Process identification.
	
    pid = fork();     //Creates a process of itself strapped to 'pid'.

   if (pid == 0)      //If 0 then we skip the loop entirely. 
   {
	   	while(1)  //Initialise infinite loop.
	   	{
	   		time ( &time_raw_format );        //Grabs the address for the time
	   		ptr_time = localtime ( &time_raw_format );
	   		syslog(LOG_EMERG, "%s", ("Current local time and date: %s", asctime(ptr_time))); //Chucks the systems time
	   		sleep(10);           															//out as a String. 
	   	}
                      //-------------PROCESS ONE ----------^
      _exit(0);
   }                
   else               
   {
   
	   	while(1) //Initialises infinte loop. 
	   	{
	   		time ( &time_raw_format );                //Does the same as above.
	   		ptr_time = localtime ( &time_raw_format );
	   		syslog(LOG_EMERG, "%s", ("Current local time and date: %s", asctime(ptr_time)));  //Does the same as above.
	   		sleep(10);
	   	}         
                  //-------------PROCESS TWO ----------^
      exit(0);
   }
   return 0;
}
