#include <sys/time.h>     //Import time types 
#include <sys/resource.h>  //Import definitions for XSI resource operations 

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

//#include "osx.c"			//timer function I made for OSX

#define SIZE 1000              //Size of Arrays 
#define ITERATIONS 100000  //Iteration times 

struct timespec start, finish;

double a[SIZE];  //Initialise array a
double b[SIZE];  //Initialise array b 


int64_t xelapsed(struct timespec *a, struct timespec *b)
{
	return (((a->tv_sec * 1000000000) + a->tv_nsec) - ((b->tv_sec * 1000000000) + b->tv_nsec)) / 100000;
}

void sequential()
{
	int i;
	int g;
	
	for(i = 0; i < ITERATIONS; i++)
	{
		for(g = 0; g < SIZE; g++)
		{
			a[g] += b[g];
		}
	}
}

void initData()
{
  int i;
  
  for( i=0; i<SIZE; i++) 
  {
    a[i] = i;
    b[i] = (SIZE-1) - i;
  }
  
}



int main()
{
	clock_gettime(CLOCK_REALTIME, &start);		//start the timer
  //current_utc_time(&start); //I wrote this for OSX

	initData();
	sequential();

	clock_gettime(CLOCK_REALTIME, &finish);		//stop the timer
  //current_utc_time(&finish); //I wrote this for OSX

	printf("Time Elapsed: %03lli microseconds\n", xelapsed(&finish, &start));

  return(0);
}
