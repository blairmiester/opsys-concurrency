#include <sys/time.h>   //Import time types
#include <sys/resource.h>  //Import definitions for XSI resource operations

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

//#include "osx.c"		//Timer function I made for OSX

#define N_THREADS 16  //Amount of threads
#define SIZE 1000 //Array capacity
#define ITERATIONS 100000 //Length of iterations 

#define MIN(a,b)  ((a) < (b)? (a): (b))

typedef struct threadArg threadArg_t;

struct timespec start, finish;

struct threadArg 
{
  pthread_t tid;  
  int from;
  int to;
};

double a[SIZE];  //Initialise array a 
double b[SIZE];  //Initialise array b 

int64_t xelapsed(struct timespec *a, struct timespec *b)
{
	return (((a->tv_sec * 1000000000) + a->tv_nsec) - ((b->tv_sec * 1000000000) + b->tv_nsec)) / 100000;
}

void * simd(void *arg)
{
	struct threadArg * ta = (struct threadArg *) arg;

	int x;
	
	for(x = ta->from; x <= ta->to; x ++)
	{
		a[x] += b[x];
	}
	
	return NULL;
}

void parallel(int threadCount)
{
	struct threadArg threads[threadCount];
	int i;
	int t;
	int threadsUsed;
	for(i = 0; i <= ITERATIONS; i++)
	{
		//Create Threads
		int threadSize = (SIZE/threadCount)+1;
		for(t = 0; t < threadCount; t ++)
		{
			threads[t].from = (t*threadSize);
			threads[t].to = (threads[t].from + threadSize);
			
			if(threads[t].from >= SIZE-1)
			{ 
				break;
			}
			if(threads[t].to >= SIZE)
			{
				threads[t].to = SIZE-1;
			}
			if(pthread_create(&(threads[t].tid), NULL, simd, (void *)&threads[t]))
			{
				fprintf(stderr, "Thread %i creation failed", t);
			}
		}
		
		threadsUsed = t;

		for(t = 0; t < threadsUsed; t ++)
		{
			pthread_join(threads[t].tid, NULL);	   //threads are joined here
		}
	}
}

void initData()
{
  int element;
  
  for( element = 0; element < SIZE; element++) 
  {
    a[element] = element;
    b[element] = (SIZE-1) - element;
  }
  
}

int main(int argc, char ** args)
{
	clock_gettime(CLOCK_REALTIME, &start);		  //start the timer
  //current_utc_time(&start); //I wrote this for OSX

	initData();
	
	int threads;						          //initialise thread counter variable
	
	if(argc == 2) 
	{
		threads = atoi(args[1]);		         //use atoi to convert to integer
	}
	else 
	{
		threads = N_THREADS;
	}
	
	parallel(threads);					         //pass in threads
	
	clock_gettime(CLOCK_REALTIME, &finish);		 //stop the timer
  //current_utc_time(&finish); //I wrote this for OSX

	printf("Total Running Time: %03lli microseconds\n", xelapsed(&finish, &start));

	return(0);
}
