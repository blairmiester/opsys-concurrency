#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/resource.h>

//#include "osx.c"		//timer c file I made for development on OSX
//#include "barrier.c"		//barrier c file I made for development on OSX

#define N_THREADS 16
#define SIZE 1000
#define ITERATIONS 100000

#define MIN( a, b)  ((a) < (b)? (a): (b))

typedef struct threadArg threadArg_t;

struct timespec start, finish;

struct threadArg 
{
  pthread_t tid;
  int from;
  int to;
};

double a[SIZE];
double b[SIZE];

pthread_barrier_t barrier;

int64_t xelapsed(struct timespec *a, struct timespec *b)
{
	return (((a->tv_sec * 1000000000) + a->tv_nsec)
		- ((b->tv_sec * 1000000000) + b->tv_nsec)) / 100000;
}

void initData(int threadCount)
{
	int i;
	
	for( i=0; i<SIZE; i++) 
	{
		a[i] = i;
		b[i] = (SIZE-1) - i;
	}
	
	pthread_barrier_init(&barrier, NULL, threadCount);
}

void * simd(void *arg)
{
	struct threadArg * ta = (struct threadArg *) arg;

	int j;
	int i;
	
	for(i = 0; i <= ITERATIONS; i++)
	{
		for(j = ta->from; j <= ta->to; j ++)
		{
			a[j] += b[j];
		}
		pthread_barrier_wait(&barrier);
	}
	
	return NULL;
}

void parallel(int threadCount)
{
	struct threadArg threads[threadCount];
	int t;
	
	int sliceSize = (SIZE/threadCount)+1;
	
	//Create Threads
	for(t = 0; t < threadCount; t ++)
	{
		threads[t].from = (t*sliceSize);
		threads[t].to = (threads[t].from + sliceSize);
		
		if(threads[t].from >= SIZE-1) break;
		if(threads[t].to >= SIZE) threads[t].to = SIZE-1;
		
		// if thread creation failed, print an error to STDERR and exit
		if(pthread_create(&(threads[t].tid), NULL, simd, (void *)&threads[t]))
		{
			fprintf(stderr, "Thread %i creation failed", t);
		}
	}
	
	//Join the threads once done
	for(t = 0; t < threadCount; t ++)
	{
		pthread_join(threads[t].tid, NULL);
	}
}



int main(int argc, char ** args)
{
	clock_gettime(CLOCK_REALTIME, &start);
	//current_utc_time(&start); //I wrote this for OSX

	int threadCount;
	if(argc == 2)
	{ 
		threadCount = atoi(args[1]);
	}
	else 
	{
		threadCount = N_THREADS;
	}
	
	initData(threadCount);
	
	parallel(threadCount);
	
	clock_gettime(CLOCK_REALTIME, &finish);
  	//current_utc_time(&start); //I wrote this for OSX

	printf("Time Elapsed: %03li microseconds\n", xelapsed(&finish, &start));

	return(0);
}
