#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include "my_malloc.h"

int main( int argc, char ** argv )
{
	const size_t GB = 1024 * 1024 * 1024;
	float max_size = 0.75f * GB;
	
	FILE * output;
	output = fopen( "test_mymalloc.txt", "w" );	
	fprintf( output, "Maximum size: %.0f\n", max_size );
	
	int * pointer;
	
	int i = 1;
	int size = 1024;
	while( size < max_size )
	{
		pointer = malloc( size );
		
		if( pointer == NULL )
		{
			fprintf( output, "Test failed!\n" );
			fclose( output );
			return -1;
		}
		
		fprintf( output, "Address: %p\tBytes: %d\tIteration: %d\n", pointer, size, i );
		
		free( pointer );

		i++;
		size += 1024;
	}
	
	fprintf( output, "Test complete!\n" );
	fclose( output );
	return 0;
}
