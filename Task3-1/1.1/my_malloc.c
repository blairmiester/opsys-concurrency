/* malloc main
 *
 * This module contains the three library functions malloc(), free() and
 * together with a helper function morecore() that asks the system
 * for more memory.
 *
 */

#include "brk.h"
#include "my_malloc.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * An empty list to get started.
 */
static Header base;

/*
 * Start of the free list
 */
static Header *freep = NULL;



/* malloc
 *
 * malloc returns the start address of dynamically allocated memory.
 *
 */
void * my_malloc(
    size_t nbytes) /* number of bytes of memory to allocate */
{
    if (nbytes == 0) {
        return NULL;
    }

	Header *p, *prevp;
    Header *moreroce(unsigned);
    unsigned nunits;

    nunits = (nbytes+sizeof(Header)-1)/sizeof(Header) + 1;
    if ((prevp = freep) == NULL) { /* no free list yet */
        base.s.ptr = freep = prevp = &base;
        base.s.size = 0;
    }


    /*
     * Iterate over the free list and find a block that is large enough to hold
     * nbytes of data.
     */

    for (p = prevp->s.ptr; ; prevp = p, p = p->s.ptr) {
        if (p->s.size >= nunits) { /* big enough */
            if (p->s.size == nunits) { /* exactly */
                prevp->s.ptr = p->s.ptr;
            }
            else {
                /* allocate tail end */
                p->s.size -= nunits;
                p += p->s.size;
                p->s.size = nunits;
            }
            freep = prevp;
            /* return Data part of block to user */
            return (void *)(p+1); 
        }
        if (p == freep) { /* wrapped around free list */
            if ((p = morecore(nunits)) == NULL)
                return NULL; /* none left */
        }
    }

}


/* morecore
 *
 * morecore asks system for more memory and returns pointer to it.
 *
 */
static Header *morecore(
    unsigned nu) /* the amount of memory to ask for (in Header-sized units) */
{
    char *cp;
    Header *up;
    if (nu < NALLOC)
        nu = NALLOC;
    cp = sbrk(nu * sizeof(Header));
    if (cp == (char *) -1) /* no space at all */
        return NULL;
    up = (Header *) cp;
    up->s.size = nu;
    my_free((void *)(up+1));
    return freep;
}


/* free
 *
 * Put block ap in free list.
 */
void my_free(
    void *ap) /* pointer to data part of allocated memory block */
{

    if (ap == NULL) {
        return;
    }

    Header *bp, *p;
    bp = (Header *)ap - 1;

    if (bp->s.size <= 0) {
        return;
    }



    /*
     * Merge free memory blocks if the one being freed is adjacent to another
     * free one,
     */

    /* point to block header */
    for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
        if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
            break; /* freed block at start or end of arena */


    if (bp + bp->s.size == p->s.ptr) {
        /* join to upper nbr */
        bp->s.size += p->s.ptr->s.size;
        bp->s.ptr = p->s.ptr->s.ptr;
    } else
        bp->s.ptr = p->s.ptr;

    if (p + p->s.size == bp) {
        /* join to lower nbr */
        p->s.size += bp->s.size;
        p->s.ptr = bp->s.ptr;
    } else
        p->s.ptr = bp;

    freep = p;
}
