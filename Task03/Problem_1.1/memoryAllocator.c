#include <unistd.h> //Library import for all the misc functions and symbolic constants and types
#include <stdio.h> //Standard IO Import
#include <time.h> //Used for measuring the time It will take to allocate memory chunks
#define GB 1073741824 /* 1GB in Bytes to be used in the following functions*/

struct chunk_info //Initialise structure containing info on memory chunk
{
        size_t size; //Such as the size of the chunk
        int on; //It's current location within the chunks memory
        struct chunk_info * next; //The pointer to what's still to come in the chunk
        struct chunk_info * prev; //The pointer to what has already been in the chunk
};

static struct chunk_info * head = NULL; //Initialise empty pointer to front of chunk
static struct chunk_info * tail = NULL; //Initilaise empty pointer to the end of the chunk

size_t chunk_size = sizeof(struct chunk_info); //Creates an unsigned type to chunk_size allowing only positive values. This equates to the size of the chunk structure

struct chunk_info * incPointer(struct chunk_info * pointer, size_t val) //Makes a pointer to tchunk info which localises a pointer to the chunk and an unsigned type to a value variable 
{
        return (struct chunk_info *)((size_t)pointer + val); //Return the current pointer of chunk info combined with the local positive value
}

void * initMalloc(size_t size) //Function to deal with the allocation of memory
{        
	tail = (struct chunk_info *) sbrk(GB); //Increments the GB in bytes using the sbrk function and then references that to the chunk info's pointer 
        
        printf("starts at: %p, chunk_size: %lu\n", (void *)tail, chunk_size); //Output the starting loaction in memory and the size
        
        head = incPointer(tail, (size + chunk_size)); //The inPointers contents, including size and size of the chunk get chucked into head
        
        head->size = GB - size - (2*chunk_size); 
        head->on = 0;
        head->next = NULL;
        
        tail->size = size;
        tail->on = 1;
        tail->next = head;
        
        return incPointer(tail, chunk_size); //Return incPointers current full contents of chunk info minus the head and the size of the chunk
}



void * my_malloc(size_t size) //This deals with the already initialised memory
{

        if(tail == NULL) return initMalloc(size); //If there is nothing in the contents of the chunk then return the size of the initialisation
        
        struct chunk_info *ci = tail; //Make ci point to chunk info and relate it to tail

        while(ci->next != NULL) { //While we have memory in the chunks tail
                if(ci->on == 0 && ci->size >= size){ //And if the current index is at the beginning and the size is greater than it's starting position
                        ci->on = 1; //Then ci and on increment to one

                        return incPointer(ci, chunk_size); //Return incPointer info 
                }
                ci = ci->next; //Increment to next part in chunk
        }
        if(head->size < (size + chunk_size)) //If the chunks head is less than the combination of the size and the chunk 
		{
                return NULL; //Return nothing
        }

        struct chunk_info * newHead = incPointer(head, (chunk_size + size));

        newHead->size = head->size - size - chunk_size; //Infers size pointer in chunk inf struct 
        newHead->on = 0; //infers on pointer to 0 in chunk info struct
        newHead->next = NULL; //Infers next pointer in chunk info to be NULL

        head->size = size; //...Same as above
        head->on = 1; //Same stuff as above
 
        head->next = newHead; //...
        struct chunk_info * oldHead = head;
        head = newHead;
 
        return incPointer(oldHead , chunk_size);
}


void my_free(void * object) //Function for what isn't being used in the chunk
{
        struct chunk_info * ci = incPointer(object, -chunk_size); //Inc Pointer is chucked into the chunks info containing the object of this function and the chunks size
        ci->on = 0; //Reference ci to zero
}
