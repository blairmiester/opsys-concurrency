#include <unistd.h>

void * my_malloc(size_t size);
void my_free(void * object);
